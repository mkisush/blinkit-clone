import { combineReducers } from "redux";
import userReducer from "./userReducer";
import locationReducer from "./locationReducer";
import productsReducer from "./productsReducer";
import cartReducer from "./cartReducer";
import searchReducer from "./searchReducer";



export default combineReducers({
    userReducer,
    locationReducer,
    productsReducer,
    cartReducer,
    searchReducer,
});
