import { SET_LOCATION } from "../actionTypes";

const initialState = {
    location: {
        deliveryTime: "11 minutes",
        deliveryLocation: "Koramangala, Bengaluru, Karnataka, India",
    },
};

const locationReducer = (state = initialState, action) => {
    let updatedState;
    switch (action.type) {

        case SET_LOCATION:
            updatedState = {
                ...state,
                location: {
                    deliveryTime: action.payload.deliveryTime,
                    deliveryLocation: action.payload.deliveryLocation,
                }

            }
            return updatedState;

        default:
            return state;
    };
}

export default locationReducer;