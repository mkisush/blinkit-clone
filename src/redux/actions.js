import {
    SIGN_IN_USER,
    SIGN_OUT_USER,
    LOAD_PRODUCT_DATA,
    ADD_PRODUCT_TO_CART,
    REMOVE_PRODUCT_FROM_CART,
    UPDATE_SEARCH_STRING,
    ADD_ADDRESS,
    REMOVE_ADDRESS,
    UPDATE_ADDRESS,

} from "./actionTypes"


//search actions
export const updateSearchString = (passedSearchString) => {
    return {
        type: UPDATE_SEARCH_STRING,
        payload: {
            searchString: passedSearchString,
        }
    };
};



//user actions
export const signInUser = (userObject) => {
    return {
        type: SIGN_IN_USER,
        payload: {
            isLoggedIn: true,
            userId: userObject.id,
            userName: userObject.userName,
            userPhoneNumber: userObject.userPhoneNumber,
        }
    };
};

export const signOutUser = () => {

    return {
        type: SIGN_OUT_USER,
        payload: {
            isLoggedIn: false,
        }
    };
};

export const addAddress = (addressObject) => {
    return {
        type: ADD_ADDRESS,
        payload: {
            addressesNickName: addressObject.nicknameId,
            receiverTitle: addressObject.title,
            receiverName: addressObject.receiverName,
            addressOfHouse: addressObject.addressOfHouse,
            addressOfStreet: addressObject.addressOfStreet,
            latitude: addressObject.latitude,
            longitude: addressObject.longitude,
        }
    };

};

export const removeAddress = () => {
    return {
        type: REMOVE_ADDRESS,
        payload: {
            addressesNickName: addressObject.nicknameId,
            receiverTitle: addressObject.title,
            receiverName: addressObject.receiverName,
            addressOfHouse: addressObject.addressOfHouse,
            addressOfStreet: addressObject.addressOfStreet,
            latitude: addressObject.latitude,
            longitude: addressObject.longitude,
        }
    };

};

export const updateAddress = () => {
    return {
        type: UPDATE_ADDRESS,
        payload: {
            addressesNickName: addressObject.nicknameId,
            receiverTitle: addressObject.title,
            receiverName: addressObject.receiverName,
            addressOfHouse: addressObject.addressOfHouse,
            addressOfStreet: addressObject.addressOfStreet,
            latitude: addressObject.latitude,
            longitude: addressObject.longitude,
        }
    };

};


//product actions
export const getProductsData = () => {
    return {
        type: LOAD_PRODUCT_DATA,
        payload: {
            readFromFile: true,
        }
    };
};



//cart actions
export const addProductToCart = (passedProductObject) => {

    return {
        type: ADD_PRODUCT_TO_CART,
        payload: {
            productId: passedProductObject.productId,
            productPrice: passedProductObject.productPrice,
        }
    };

};

export const removeProductFromCart = (passedProductObject) => {

    return {
        type: REMOVE_PRODUCT_FROM_CART,
        payload: {
            productId: passedProductObject.productId,
            productPrice: passedProductObject.productPrice,
        }
    };

};

