import React, { Component } from 'react';
import { connect } from 'react-redux';
import "../styles/LoginSignup.css"
import { SIGN_IN_USER } from "../redux/actionTypes"
import validator from 'validator';

class LoginSignup extends Component {

    constructor(props) {
        super(props);

        this.login_form_states = {
            WAITING: "waiting",
            ENTER_NUMBER: "enter_number",
            ENTER_OTP: "enter-otp",
            SUCCESSFUL: "successful",
            ERROR: "error",

        };

        this.state = {
            inputPhoneNumber: null,
            isNumberValid: false,
            inputOTP: {
                first: null,
                second: null,
                third: null,
                fourth: null,
            },
            isOTPValid: false,
            timerForResendOTP: null,
            login_form_status: this.login_form_states.ENTER_NUMBER,
        }
    }

    handleUserInput = (event) => {

        if (event.target.id === "phone-no-input-box") {

            this.setState({
                inputPhoneNumber: event.target.value,

            },
                () => {
                    this.validateFormInput(event.target.id);

                }
            );

        }

        if (event.target.id === "login-next-button-number" && this.state.isNumberValid === true) {

            this.setState({
                login_form_status: this.login_form_states.ENTER_OTP,
            })

        }

        if (event.target.id === "login-next-button-otp" && this.state.isOTPValid === true) {

            this.setState({
                login_form_status: this.login_form_states.SUCCESSFUL,
            });

        }

        if (event.target.id === "back-to-enter-number") {

            this.setState({
                login_form_status: this.login_form_states.ENTER_NUMBER,
                inputPhoneNumber: null,
                isNumberValid: false,
                inputOTP: {
                    first: null,
                    second: null,
                    third: null,
                    fourth: null,
                },
                isOTPValid: false,
            })

        }

        if (event.target.id === "first"
            || event.target.id === "second"
            || event.target.id === "third"
            || event.target.id === "fourth") {

            this.setState((prevState) => {

                return {
                    inputOTP: {
                        ...prevState.inputOTP,
                        [event.target.id]: event.target.value,
                    }
                }

            },
                () => {
                    this.validateFormInput(event.target.id);

                }
            );

        }



    };

    validateFormInput = (idOfInput) => {
        if (idOfInput === "phone-no-input-box") {
            let isValid = false;
            this.setState(() => {
                if (this.state.inputPhoneNumber.length === 10 &&
                    validator.isNumeric(this.state.inputPhoneNumber) === true) {
                    isValid = true;
                }

                return {
                    isNumberValid: isValid,
                }

            })

        }

        if (idOfInput === "first"
            || idOfInput === "second"
            || idOfInput === "third"
            || idOfInput === "fourth") {

            let isValid = false;

            this.setState((prevState) => {
                if (prevState.inputOTP.first === null ||
                    prevState.inputOTP.second === null ||
                    prevState.inputOTP.third === null ||
                    prevState.inputOTP.fourth === null
                ) {

                    isValid = false;
                } else {
                    if (validator.isNumeric(prevState.inputOTP.first) &&
                        validator.isNumeric(prevState.inputOTP.second) &&
                        validator.isNumeric(prevState.inputOTP.third) &&
                        validator.isNumeric(prevState.inputOTP.fourth)) {
                        isValid = true;
                    }

                }

                return {
                    isOTPValid: isValid,
                }
            })

        }

    }



    render() {
        return (
            <div className="pl-2">
                <div className="modal"
                    id="logInModalCenter"
                    tabIndex="-1"
                    role="dialog"
                    aria-labelledby="logInModalCenterTitle"
                    aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered"
                        role="document">
                        <div className="modal-content p-4 row">

                            {
                                this.state.login_form_status === this.login_form_states.ENTER_OTP &&
                                <div className="col-1 p-0">
                                    <span
                                        id="back-to-enter-number"
                                        onClick={(event) => { this.handleUserInput(event) }}>
                                        Back
                                    </span>
                                </div>
                            }

                            {
                                this.state.login_form_status !== this.login_form_states.SUCCESSFUL &&
                                <section className="d-flex signup-heading justify-content-center border-bottom m-auto col-12">
                                    <h2 className="signup-form-heading">Phone Number Verification</h2>
                                </section>
                            }

                            {
                                this.state.login_form_status === this.login_form_states.ENTER_NUMBER &&
                                <>
                                    <section className="d-flex flex-column m-auto">
                                        <p className="text-center">Enter your phone number to</p>
                                        <p className="text-center">Login/Sign up</p>
                                    </section>

                                    <section className="d-flex flex-row input-section justify-self-center input-box m-auto">
                                        <span className="prefix">
                                            <i className="fa-solid fa-mobile-screen-button"></i>
                                            &nbsp;
                                            +91 -
                                        </span>
                                        <input type="tel"
                                            maxLength="10"
                                            className="login-phone__input input"
                                            id="phone-no-input-box"
                                            onChange={(event) => { this.handleUserInput(event) }} />
                                    </section>

                                    <section className="input-section justify-self-center button-box mt-2 mx-auto">
                                        <button
                                            className={
                                                this.state.isNumberValid === false ?
                                                    "btn weight--semibold login-button btn-secondary" :
                                                    "btn weight--semibold login-button btn-success"
                                            }
                                            id="login-next-button-number"
                                            onClick={(event) => { this.handleUserInput(event) }}>
                                            Next
                                        </button>
                                    </section>

                                    <div className="PhoneNumberLogin  mx-auto">
                                        <div>By continuing, you agree to our</div>
                                        <div>
                                            <a target="_blank"
                                                href="/terms"
                                                className="tos-privacy-links">
                                                Terms of service
                                            </a>
                                            &nbsp;&nbsp;
                                            <a target="_blank"
                                                href="/privacy"
                                                className="tos-privacy-links">
                                                Privacy policy
                                            </a>
                                        </div>
                                    </div>
                                </>

                            }

                            {
                                this.state.login_form_status === this.login_form_states.ENTER_OTP &&
                                <>
                                    <section className="d-flex flex-column mx-auto">
                                        <p className="text-center">Enter 4 digit code sent to your phone</p>
                                        <p className="text-center">+91-{this.state.inputPhoneNumber}</p>
                                    </section>

                                    <section className="d-flex flex-row input-section justify-self-center m-auto">

                                        <div id="otp"
                                            className="inputs d-flex flex-row justify-content-center mt-2 mx-auto">
                                            <input className="m-2 text-center form-control rounded"
                                                type="tel"
                                                id="first"
                                                maxLength="1"
                                                onChange={(event) => {
                                                    this.handleUserInput(event)
                                                }} />
                                            <input className="m-2 text-center form-control rounded"
                                                type="tel" id="second"
                                                maxLength="1"
                                                onChange={(event) => {
                                                    this.handleUserInput(event)
                                                }} />
                                            <input className="m-2 text-center form-control rounded"
                                                type="tel"
                                                id="third"
                                                maxLength="1"
                                                onChange={(event) => {
                                                    this.handleUserInput(event)
                                                }} />
                                            <input className="m-2 text-center form-control rounded"
                                                type="tel"
                                                id="fourth"
                                                maxLength="1"
                                                onChange={(event) => {
                                                    this.handleUserInput(event)
                                                }} />
                                        </div>
                                    </section>

                                    <section className="input-section justify-self-center button-box mt-2 mx-auto">
                                        <button
                                            className={
                                                this.state.isOTPValid === false ?
                                                    "btn weight--semibold login-button btn-secondary" :
                                                    "btn weight--semibold login-button btn-success"
                                            }
                                            id="login-next-button-otp"
                                            onClick={(event) => { this.handleUserInput(event) }}>
                                            Next
                                        </button>
                                    </section>


                                    <section className='mx-auto'>
                                        <div className="countdown-text">
                                            <a className="otp-resend otp-resend--disabled"
                                                id="resend-otp"
                                                href="noreferrer noopener"
                                            >Resend Code {this.state.timerForResendOTP}
                                            </a>
                                        </div>
                                    </section>


                                </>

                            }

                            {
                                this.state.login_form_status === this.login_form_states.SUCCESSFUL &&
                                <section className="row">
                                    <span
                                        type="button"
                                        className="btn-close col-1 align-self-end"
                                        data-dismiss="modal"
                                        aria-label="Close"
                                        onClick={() => {
                                            this.props.signInUser({
                                                isLoggedIn: true,
                                                userId: "userId_placeholder_",
                                                userPhoneNumber: this.state.inputPhoneNumber,
                                            });
                                        }}
                                    >
                                        X
                                    </span>
                                    <section className="d-flex flex-column successful-heading col-12">
                                        <i className="fa-solid fa-circle-check successful-tick text-center"></i>
                                        <p className="text-center mt-1">Successfully logged in!</p>
                                    </section>
                                </section>
                            }

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = {
    signInUser: (payload) => {
        return {
            type: SIGN_IN_USER,
            payload,
        }

    }
};

export default connect(null, mapDispatchToProps)(LoginSignup);

