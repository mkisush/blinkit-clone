import React, { Component } from 'react';
import { connect } from 'react-redux';
import "../styles/ProductCard.css";
import {
    ADD_PRODUCT_TO_CART,
    REMOVE_PRODUCT_FROM_CART
} from "../redux/actionTypes"

class ProductCard extends Component {

    render() {
        return (
            <div className="card product-card p-1">
                <img className="card-img-top"
                    src={this.props.productData.images[0]}
                    alt="" />
                <div className="card-body">
                    <p className="card-title">
                        {this.props.productData.title}
                    </p>
                    <p className="card-quantity">{this.props.productData.quantity.count} {this.props.productData.quantity.unit}</p>
                    <span className='d-flex justify-content-between'>


                        {
                            this.props.productData.discount > 0 ?

                                <span className="card-price d-flex flex-column">
                                    <span className="actual-price">
                                        &#8377;{this.props.productData.price * (1 - this.props.productData.discount)}
                                    </span>
                                    <span className="crossed-out">
                                        &#8377;{this.props.productData.price}
                                    </span>
                                </span>

                                :

                                <span className="card-price d-flex flex-column">
                                    <span className="actual-price">
                                        &#8377;{this.props.productData.price}
                                    </span>
                                </span>


                        }

                        {

                            (this.props.productData.id in this.props.cartData) ?

                                <div className="add-button btn btn-light btn-sm w-50 d-flex justify-content-between align-items-center">
                                    <a className="decrement-product"
                                        onClick={() => {
                                            this.props.removeProductFromCart({
                                                productId: this.props.productData.id,
                                                productPrice: this.props.productData.price * (1 - this.props.productData.discount),
                                            });

                                        }}>
                                        &#xFF0D;
                                    </a>
                                    <span>{this.props.cartData[this.props.productData.id]}</span>
                                    <a className="increment-product"
                                        onClick={() => {
                                            this.props.addProductToCart({
                                                productId: this.props.productData.id,
                                                productPrice: this.props.productData.price * (1 - this.props.productData.discount),
                                            });

                                        }}>
                                        &#xFF0B;
                                    </a>
                                </div>

                                :
                                <button className="add-button btn btn-light btn-sm w-50"
                                    onClick={() => {
                                        this.props.addProductToCart({
                                            productId: this.props.productData.id,
                                            productPrice: this.props.productData.price * (1 - this.props.productData.discount),
                                        });
                                    }}>
                                    ADD
                                </button>


                        }

                    </span>
                </div>
            </div>

        );
    }
}

const mapStateToProps = (state) => {

    const cartDataFromStore = state.cartReducer.cartItems;
    return {
        cartData: cartDataFromStore,
    };
};
const mapDispatchToProps = {
    addProductToCart: (payload) => {
        return {
            type: ADD_PRODUCT_TO_CART,
            payload,
        }

    },
    removeProductFromCart: (payload) => {
        return {
            type: REMOVE_PRODUCT_FROM_CART,
            payload,
        }

    },
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductCard);