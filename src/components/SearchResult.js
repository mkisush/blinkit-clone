import React, { Component } from 'react';
import ProductCard from './ProductCard';
import { connect } from 'react-redux';
import dog_image from "../asset/dog-nothing.png";

class SearchResult extends Component {

    render() {
        console.log(this.props.productData);
        return (
            <div className="container-fluid row justify-content-center products-HomePage">

                {
                    this.props.searchString === "" &&
                    <>
                        <h6 className="m-auto col-12 text-center">
                            <i class="fa-solid fa-arrow-trend-up"></i>
                            {" "}
                            Trending
                        </h6>
                        <div className="mt-4 col-11 row ">
                            {
                                this.props.trendingProducts.map((singleProductObject) => {

                                    return (
                                        <div key={singleProductObject.id}
                                            className="m-auto col-3">
                                            <ProductCard productData={singleProductObject} />
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </>
                }

                {
                    (this.props.searchString !== "" &&
                        Array.isArray(this.props.productData) &&
                        this.props.productData.length > 0) &&
                    <>
                        <h5>Showing results for "{this.props.searchString}"</h5>
                        <div className="mt-4 col-11 row ">
                            {
                                this.props.productData.map((singleProductObject) => {

                                    return (
                                        <div key={singleProductObject.id}
                                            className="m-auto col-3">
                                            <ProductCard productData={singleProductObject} />
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </>

                }

                {

                    (this.props.searchString !== "" &&
                        Array.isArray(this.props.productData) &&
                        this.props.productData.length === 0) &&
                    <div className="text-center">

                        <img className="w-50 h-50"
                            src={dog_image}
                            alt="" />
                        <h1>Sorry no results for {this.props.searchString}</h1>
                    </div>
                }


            </div>
        );
    }
}
const mapStateToProps = (state) => {

    const productDataFromStore = state.productsReducer.products;
    const searchString = state.searchReducer.search.searchString.toLowerCase();
    const trendingProducts = state.productsReducer.products.slice(3, 7);

    const searchResultProducts = productDataFromStore.filter((singleProductObject) => {
        if (
            singleProductObject.title.toLowerCase().indexOf(searchString) > -1 ||
            singleProductObject.brand.toLowerCase().indexOf(searchString) > -1
        ) {
            return true;

        } else {
            return false;
        }

    });

    return {
        productData: searchResultProducts,
        searchString: state.searchReducer.search.searchString,
        trendingProducts,
    };
};

export default connect(mapStateToProps)(SearchResult);