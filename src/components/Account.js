import React, { Component } from 'react';
import { connect } from 'react-redux';
import "../styles/LoginSignup.css";
import { SIGN_OUT_USER } from "../redux/actionTypes";

class Account extends Component {

    constructor(props) {
        super(props);


    }

    handleUserInput = (event) => {

        if (event.target.id === "phone-no-input-box") {

            this.setState({
                inputPhoneNumber: event.target.value,

            },
                () => {
                    this.validateFormInput(event.target.id);

                }
            );

        }

        if (event.target.id === "login-next-button-number" && this.state.isNumberValid === true) {

            this.setState({
                login_form_status: this.login_form_states.ENTER_OTP,
            })

        }

        if (event.target.id === "login-next-button-otp" && this.state.isOTPValid === true) {

            this.setState({
                login_form_status: this.login_form_states.SUCCESSFUL,
            });

        }

        if (event.target.id === "back-to-enter-number") {

            this.setState({
                login_form_status: this.login_form_states.ENTER_NUMBER,
                inputPhoneNumber: null,
                isNumberValid: false,
                inputOTP: {
                    first: null,
                    second: null,
                    third: null,
                    fourth: null,
                },
                isOTPValid: false,
            })

        }

        if (event.target.id === "first"
            || event.target.id === "second"
            || event.target.id === "third"
            || event.target.id === "fourth") {

            this.setState((prevState) => {

                return {
                    inputOTP: {
                        ...prevState.inputOTP,
                        [event.target.id]: event.target.value,
                    }
                }

            },
                () => {
                    this.validateFormInput(event.target.id);

                }
            );

        }



    };



    render() {
        return (
            <>
                <div class="collapse" id="collapseAccount">
                    <ul className="list-unstyled text-center">
                        <li>
                            <p>My Account</p>
                            <p>{this.props.user.userPhoneNumber}</p>
                        </li>

                        <li>My Orders</li>
                        <li>Saved Address</li>
                        <li>My Wallet
                            ₹0</li>
                        <li>FAQ's</li>
                        <li
                            type="button"
                            data-toggle="collapse"
                            href="#collapseAccount"
                            onClick={() => {
                                this.props.signOutUser();
                            }}>Log Out</li>
                    </ul>
                </div>

            </>
        )
    }
}

const mapStateToProps = (state) => {

    const user = state.userReducer.user;
    const deliveryTime = state.locationReducer.location.deliveryTime;
    const deliveryLocation = state.locationReducer.location.deliveryLocation;

    return {
        deliveryTime,
        deliveryLocation,
        user,

    };
};

const mapDispatchToProps = {
    signOutUser: () => {
        return {
            type: SIGN_OUT_USER,
        }

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Account);